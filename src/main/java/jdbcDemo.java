import java.sql.*;

public class jdbcDemo {
    public static void main(String[] args) {
        System.out.println("JDBC Demo");

        selectAllDemo();
       // insertStudentDemo("Hansi Hinterseher", "hhinter@gmail.com");
       // updateStudentDemo(8,"Seas Gaugi", "seas@org.at");

        System.out.println("'''''''''''''''''");
        findAllByNameLike("i");
        System.out.println("'''''''''''''''''");
        //deleteStudentDemo(7);
        selectAllDemo();
    }

    public static void findAllByNameLike(String nameMit){
        System.out.println("Select DEMO mit JDBC");
        String sqlSelectAllPersons = "SELECT * FROM `student` WHERE `student`.`name` LIKE ?";
        String connectionUrl = "jdbc:mysql://localhost:3306/jdbcdemo";
        String user = "root";
        String password = "";

        try (Connection conn = DriverManager.getConnection(connectionUrl, user , password )){
            System.out.println("Verbindung zur Datenbank hergestellt!");
            PreparedStatement preparedStatement = conn.prepareStatement(sqlSelectAllPersons);
            preparedStatement.setString(1, "%"+nameMit+"%");
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()){
                int id = rs.getInt(1);
                String name = rs.getString(2);
                String email = rs.getString(3);
                System.out.println("Student aus der DB: [ID:] " + id + " [NAME:] " + name + " [EMAIL:] " + email);
            }
        }catch (SQLException exception){
            System.out.println("Fehler bei Verbindung zur Datenbank! " + exception.getMessage());
        }
    }

    public static void deleteStudentDemo(int studentID){
        System.out.println("DELETE DEMO mit JDBC");
        String sqlInsertAllPersons = "DELETE FROM `student` WHERE `student`.`id` = ?";
        String connectionUrl = "jdbc:mysql://localhost:3306/jdbcdemo";
        String user = "root";
        String password = "";

        try (Connection conn = DriverManager.getConnection(connectionUrl, user , password )){
            System.out.println("Verbindung zur Datenbank hergestellt!");
            PreparedStatement preparedStatement = conn.prepareStatement(sqlInsertAllPersons);

            try {
                preparedStatement.setInt(1, studentID);
                int rowAffected = preparedStatement.executeUpdate();
                System.out.println("Anzahl der Aktualisierten Datensätze " + rowAffected);
            }catch (SQLException e){
                System.out.println("Fehler im SQL-DELETE-Statement" + e.getMessage());
            }
        }catch (SQLException exception){
            System.out.println("Fehler bei Verbindung zur Datenbank! " + exception.getMessage());
        }
    }

    public static void updateStudentDemo(int id,String nameStudent, String emailStudent){
        System.out.println("UPDATE DEMO mit JDBC");
        String sqlInsertAllPersons = "UPDATE `student` SET `name` = ?, `email` = ? WHERE `student`.`id` = ?";
        String connectionUrl = "jdbc:mysql://localhost:3306/jdbcdemo";
        String user = "root";
        String password = "";

        try (Connection conn = DriverManager.getConnection(connectionUrl, user , password )){
            System.out.println("Verbindung zur Datenbank hergestellt!");
            PreparedStatement preparedStatement = conn.prepareStatement(sqlInsertAllPersons);

            try {
                preparedStatement.setString(1, nameStudent);
                preparedStatement.setString(2,emailStudent);
                preparedStatement.setInt(3,id);
                int affectedRows = preparedStatement.executeUpdate();
                System.out.println("Anzahl der Aktualisierten Datensätze " + affectedRows);
            }catch (SQLException e){
                System.out.println("Fehler im SQL-UPDATE-Statement" + e.getMessage());
            }
        }catch (SQLException exception){
            System.out.println("Fehler bei Verbindung zur Datenbank! " + exception.getMessage());
        }
    }

    public static void insertStudentDemo(String name, String email){
        System.out.println("INSERT DEMO mit JDBC");
        String sqlInsertAllPersons = "INSERT INTO `student` (`id`, `name`, `email`) VALUES (NULL,?,?)";
        String connectionUrl = "jdbc:mysql://localhost:3306/jdbcdemo";
        String user = "root";
        String password = "";

        try (Connection conn = DriverManager.getConnection(connectionUrl, user , password )){
            System.out.println("Verbindung zur Datenbank hergestellt!");
            PreparedStatement preparedStatement = conn.prepareStatement(sqlInsertAllPersons);

            try {
                preparedStatement.setString(1, name);
                preparedStatement.setString(2, email);
                int rowAffected = preparedStatement.executeUpdate();
                System.out.println(rowAffected +  " Datensätze eingefügt");
            }catch (SQLException e){
                System.out.println("Fehler im SQL-INSERT-Statement" + e.getMessage());
            }
        }catch (SQLException exception){
            System.out.println("Fehler bei Verbindung zur Datenbank! " + exception.getMessage());
        }
    }

    public static void selectAllDemo(){
        System.out.println("Select DEMO mit JDBC");
        String sqlSelectAllPersons = "SELECT * FROM `student`";
        String connectionUrl = "jdbc:mysql://localhost:3306/jdbcdemo";
        String user = "root";
        String password = "";

        try (Connection conn = DriverManager.getConnection(connectionUrl, user , password )){
            System.out.println("Verbindung zur Datenbank hergestellt!");
            PreparedStatement preparedStatement = conn.prepareStatement(sqlSelectAllPersons);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()){
              int id = rs.getInt(1);
              String name = rs.getString(2);
              String email = rs.getString(3);
              System.out.println("Student aus der DB: [ID:] " + id + " [NAME:] " + name + " [EMAIL:] " + email);
            }
        }catch (SQLException exception){
            System.out.println("Fehler bei Verbindung zur Datenbank! " + exception.getMessage());
        }
    }
}
