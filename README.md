# JDBS INTRO TEIL 1

## Entwicklungsumgebung einrichten

Wir benötigen:

- Webserver (Apache)
- Datenbankserver (MySQL) läuft über den Apache / php-Progamm
- IDE (InteliJ)
- Maven-Projektsetup (Build-Tool zum Compilieren, Testen, Bauen, Dependency Managen)
    - Aufbau von Maven-Projekten immer src/main/java

Wir verwenden XAMPP

Dashboard (Version, Beschreibung, ...)

- [http://localhost/dashboard/](http://localhost/dashboard/)

Administrations Konsolle für MySQL (Datenbankserver)

- [http://localhost/phpmyadmin/](http://localhost/phpmyadmin/)

⇒ Wir erstellen ein neues Projekt und wählen Maven aus.

- Auswählen Version 17
- Next

⇒ Projektname jdbcdemo

- Finish

pom.xml

ist unsere Konfigurations-Datei unsere Dependency-Datei

Hinzufügen der Dependency für die Datenbank-Connectifity (Bibliothek)

[https://mvnrepository.com](https://mvnrepository.com/)

⇒ Suchen nach JDBC MySQL Connector/J

⇒ auf die neuste Version klicken und kopieren

⇒ einfügen in die pom.xml

```php
<dependency>
    <groupId>mysql</groupId>
    <artifactId>mysql-connector-java</artifactId>
    <version>8.0.27</version>
</dependency>
```

Bei External Libaries sieht man die externen Bibliotheken (mysql)

- neue Klasse jdbcDemo mit main-Methode

---

## Datenverbindung herstellen

von Java zu MySql-Datenbank

- Apache und MySQL muss immer gestartet sein

⇒ neue Datenbank in MyAdmin erstellen

⇒  Name der Datenbank jdbcdemo

⇒  wir erzeugen eine neue Tabelle

![img.png](img/img.png)

Spalte 1:

id (Primary, Auto_Increment)

Spalte 2:

name (Varchar, 200)

Spalte 3:

email(Varchar, 200)

```php
INSERT INTO `student` (`id`, `name`, `email`) VALUES 
(NULL, 'Dario Skocibusic', 'dario@skocibusic.at')
```

Wir machen nichts anderes als, wie über JDBC in Java Insert-Befehle abzusetzen, damit die Datenbank diese Sachen einfügen kann.

⇒ über SQL in MyAdmin können wir einen Befehl absetzen.

⇒ erstellen neuer Methode `selectAllDemo`

- In den sqlSelectAllPersons String wird der Befehl für die Abfrage in der Datenbank enthält.

```java
String sqlSelectAllPersons = "SELECT * FROM `student`";
```

Über welche URL ist der Datenbankserver erreichbar?

Verbindung (Connection):

Standard + am Ende Datenbankname:

String connectionUrl =  `jdbc:mysql://localohost:3306/jdbcdemo`

### Test Datenbankverbindung:

1. Versuch fehlgeschlagen, da ein Fehler in der URL war.

![img_1.png](img/img_1.png)

1. Versuch Verbindung wurde hergestellt.

![img_2.png](img/img_2.png)

```java
public static void selectAllDemo(){
        System.out.println("Select DEMO mit JDBC");
				//SQL-Befehl 
        String sqlSelectAllPersons = "SELECT * FROM `student`";
				//URL für die Verbindung zur Datenbank
        String connectionUrl = "jdbc:mysql://localhost:3306/jdbcdemo";
				//username
        String user = "root";
				//passwort
        String password = "";
        
				//Versuche die Connection zur Datenbank herzustellen mit dem DriverManager
        try (Connection conn = DriverManager.getConnection(connectionUrl, user , password )){
            System.out.println("Verbindung zur Datenbank hergestellt!");

        }catch (SQLException exception){
				//Bei Fehler fange die SQLExeption und gib Message
            System.out.println("Fehler bei Verbindung zur Datenbank! " + exception.getMessage());

        }
    }
```

---

`selectAllDemo()`

## Datenabfragen

Um Daten abzufragen, legen wir uns zuerst ein PreparedStatement an.

An der Verbindung rufen wir die Methode prepareStatement auf und geben dieser Methode den SQL-String mit dem Befehl mit.

`PreparedStatement preparedStatement = conn.prepareStatement(sqlSelectAllPersons);`

Abfrage an Datenbank schicken mit executeQuery() (führen die Abfrage aus):

`preparedStatement.executeQuery();`

Das Ergebnis, dass wir zurück bekommen ist ein ResultSet.

`ResultSet rs = preparedStatement.executeQuery();`

### ResultSet

Hat schon einige Methoden, eine der wichtigsten ist das next():

Wir iterieren über das ResultSet drüber.

Wir können uns das ResultSet vorstellen wie ein Zeiger, dieser Zeiger zeigt am Anfang auf nichts. Wenn ich einmal next Aufrufe zeigt er auf den ersten Datensatz, wenn ich das zweite Mal aufrufe auf den 3ten Datensatz beim 3ten mal auf den vierten Datensatz, wenn ich das fünfte Mal next() aufrufe und ich keinen Datensatz mehr habe, bekommt er einen boolischen Wert false zurück.

Somit ist das next dazu da, dass es uns sagt gibt es noch einen Datensatz und hole mir den nächsten Datensatz.

Wie bekommen wir die einzelnen Daten aus dem Datensatz?

```java
try (Connection conn = DriverManager.getConnection(connectionUrl, user , password )){
    System.out.println("Verbindung zur Datenbank hergestellt!");
		//Erzeugen eines PreparedStatement / an der Verbindung conn rufen wir
		//die Methode prepareStatement auf und geben das Statement mit
    PreparedStatement preparedStatement = conn.prepareStatement(sqlSelectAllPersons);
		// an dem prpearedStatment rufen wir executeQuery auf (Abfrage)
    ResultSet rs = preparedStatement.executeQuery();
		//an dem ResultSet rufen wir die Methode next auf (gibt uns die nächste Zeile)
    while (rs.next()){
		//gib uns den Int Wert aus der Spalte id (geht auch mit 1 für Spalten index)
    int id = rs.getInt("id");
	  //gib uns den String Wert aus der Spalte name (geht auch mit 2 für Spalten index)
    String name = rs.getString("name");
		//gib uns den String Wert aus der Spalte email (geht auch mit 3 für Spalten index)
    String email = rs.getString("email");
    System.out.println("Student aus der DB: [ID:] " + id + " [NAME:] " + name + " [EMAIL:] " + email);
      }
     }catch (SQLException exception){
     System.out.println("Fehler bei Verbindung zur Datenbank! " + exception.getMessage());
     }
 }
```

<aside>
💡 Bei Einfügen in der Datenbank aufpassen auf Absätze, diese werden in die Datenbank übernommen. (Erste Ausgabe von den Datensätzen, Abstand zwischen Datensatz 2 und 3)
Wurde Verbessert

</aside>

Wir benötigen einen try-catch -Block, da immer eine SQL-Exception bei der Verbindung auftreten kann.

---

## Daten einfügen

⇒ neue statische Methode `insertStudentDemo`

für Daten einfügen.

setMethoden

- Wir haben ein prepareStatement und dieses wird schon vor compiliert in der Datenbank abgelegt.
- Beim eigentlichen Aufruf an die Datenbank wird nur noch am fertigen INSERT-Statement werden nur noch die Datenwerte die wir mitgeben übergeben.
- Hat den Vorteil, dass es Security-Technisch besser ist, da der Client statt dem Username kein SELECT-Statement mitgeben kann.
    - Dies wäre sehr dramatisch, da die Datenbank gelöscht oder Daten abgefragt werden könnten. (SQL-Injection)
- Daher verwenden wir preparedStatement, da der Befehl schon vorab compiliert wird und nicht in der Datenbank erst.
- Es werden nur noch die Datenwerte mitgegeben für die Verarbeitung. (nur noch zB. als Strings interbretiert und nicht als SQL)

Sobald wir etwas in der Datenbank verändern, verwenden wir`.executeUpdate()`

neuer Typ-Catch Block, zum Überprüfen, ob bei dem Absetzen des SQL-Befehls ein Problem aufgetreten ist.

try()

`try (Connection conn = DriverManager.getConnection(connectionUrl, user , password )){`

Wenn wir in den Klammern die Connection des Typ-Blocks erstellen, wird die Connection automatisch geschlossen, wenn diese fertig ist.

---

## Daten aktualisieren

Wie kommen wir zu einem Update Statement?

Der Ansatz ist immer der Gleiche.

- wir sehen uns die Datenbank an.
- Können den Datensatz in der Datenbank UPDATEN und den SQL-Befehl für unser Java-Programm verwenden.

Wenn wir im Update-Befehl keine WHERE-Klausel machen würden, wird jeder gespeicherte Stundent geändert, so beschränken wir die Änderung nur auf einen mit der ID.

UPDATE-Statement:

```sql
"UPDATE `student` SET `name` = ?, `email` = ? WHERE `student`.`id` = 8"
```

---

## Datensätze löschen

⇒ neue Methode deleteStudentDemo(int studentID)

Wir geben als Parameter die ID mit (Datentyp int)

Wir verwenden im SQL-Statement wieder den Platzhalter ? für die ID

- an dem preparedStatement prüfen wir die Methode `setInt`  auf und setzen den  Int Wert
- an dem preparedStatement rufen wir die Methode executeUpdate auf und es wird in der Datenbank ausgeführt.

DELETE-Statement:

```sql
DELETE FROM `student` WHERE `student`.`id` = ?
```

---

## Speziellere SELECT-Statements

Dem LIKE-Operator ist egal, ob etwas groß oder klein geschrieben ist.

SELECT-Statement:

```sql
SELECT * FROM `student` WHERE `student`.`name` LIKE ?
```

Die Mitgabe in die Datenbank:

```java
preparedStatement.setString(1, "%"+nameMit+"%");
```

Wichtig der Parameter darf nicht name heissen sondern muss sich abweichen, sonst wirft das Programm einen Fehler.